(function($) {

    "use strict";

    $(document).ready(function() {

        /* Logo check */
        blognetwork_logo_setup();

        /* EU Cookie check */
        blognetwork_cookie_setup();

        function blognetwork_cookie_setup() {
            var cookiestatus = $.cookie('eu-cookie');
            
            if (!cookiestatus) {
                $('#cookie-bar').removeClass('hide');                
            } else {
                $('#cookie-bar').slideUp(150,function(){$('#cookie-bar').remove();});      
            }
            
            $('#cookie-bar .cb-enable').on('click', function(e) {
                e.preventDefault();                
                $('#cookie-bar').slideUp(150,function(){$('#cookie-bar').remove();});                            
                $.cookie('eu-cookie', 'true', { expires : 365, path : '/' });               
            });
        }


        /* Sticky header */
        if (blognetwork_js_settings.header_sticky) {

            var blognetwork_last_top;

            if ($('#wpadminbar').length && $('#wpadminbar').is(':visible')) {
                $('.blognetwork-sticky-header').css('top', $('#wpadminbar').height());
            }

            $(window).scroll(function() {

                var top = $(window).scrollTop();

                if (blognetwork_js_settings.header_sticky_up) {
                    if (blognetwork_last_top > top && top >= blognetwork_js_settings.header_sticky_offset) {
                        if (!$("body").hasClass('blognetwork-header-sticky-on')) {
                            $("body").addClass("blognetwork-sticky-header-on");
                        }
                    } else {
                        $("body").removeClass("blognetwork-sticky-header-on");
                        $('.blognetwork-sticky-header .blognetwork-action-search.active i').addClass('fv-search').removeClass('fv-close');
                        $('.blognetwork-sticky-header .blognetwork-actions-button').removeClass('active');

                    }
                } else {
                    if (top >= blognetwork_js_settings.header_sticky_offset) {
                        if (!$("body").hasClass('blognetwork-header-sticky-on')) {
                            $("body").addClass("blognetwork-sticky-header-on");
                        }
                    } else {
                        $("body").removeClass("blognetwork-sticky-header-on");
                        $('.blognetwork-sticky-header .blognetwork-action-search.active i').addClass('fv-search').removeClass('fv-close');
                        $('.blognetwork-sticky-header .blognetwork-actions-button').removeClass('active');

                    }
                }

                blognetwork_last_top = top;
            });
        }

        /* Top bar height check and admin bar fixes*/

        var blognetwork_admin_top_bar_height = 0;
        blognetwork_top_bar_check();

        function blognetwork_top_bar_check() {
            if ($('#wpadminbar').length && $('#wpadminbar').is(':visible')) {
                blognetwork_admin_top_bar_height = $('#wpadminbar').height();

            }

            //blognetwork_responsive_header();

        }

        /*function blognetwork_responsive_header() {

            if ($('.blognetwork-responsive-header').length) {

                $('.blognetwork-responsive-header').css('top', blognetwork_admin_top_bar_height);



                if (blognetwork_admin_top_bar_height > 0 && $('#wpadminbar').css('position') == 'absolute') {

                    if ($(window).scrollTop() <= blognetwork_admin_top_bar_height) {
                        $('.blognetwork-responsive-header').css('position', 'absolute');
                    } else {
                        $('.blognetwork-responsive-header').css('position', 'fixed').css('top', 0);
                    }

                }

            }
        }*/

        /*$(window).scroll(function() {

            blognetwork_responsive_header();

        });*/


        /* Secondary menus in responsive menu */
        if (blognetwork_js_settings.rensponsive_secondary_nav) {

            var mobile_nav = $('.blognetwork-mob-nav');
            var secondary_navigation = $('.secondary-navigation');
            var secondary_nav_html = '';
            if (secondary_navigation.length) {

                secondary_navigation.each(function() {
                    secondary_nav_html += $(this).find('ul:first').html();
                });

                if (blognetwork_js_settings.responsive_more_link) {
                    secondary_nav_html = '<li class="menu-item menu-item-has-children"><a href="#" class="blognetwork-menu-forward"><a href="#">' + blognetwork_js_settings.responsive_more_link + '</a></a><ul class="sub-menu">' + secondary_nav_html + '</ul></li>';
                }

                mobile_nav.append(secondary_nav_html);
            }
        }

        if (blognetwork_js_settings.responsive_social_nav) {

            var mobile_nav = $('.blognetwork-mob-nav');
            var social_nav = $('.blognetwork-soc-menu:first li').clone();
            var soc_nav_html = '';
            if (social_nav.length) {
                social_nav.each(function() {
                    soc_nav_html += $(this).html();
                });
                mobile_nav.append('<li class="blognetwork-soc-menu blognetwork-soc-responive-menu menu-item">' + soc_nav_html + '</li>');
            }
        }

        

            
            

         /* Check if Device is android and hide self-hosted player */

        /*if (/Android/i.test(navigator.userAgent)) {
            
            $('body').on('click', '.mejs-playpause-button button, .mejs-overlay-play', function(event) {
            var target = $( event.target );
                
                if ( target.is( "button" ) || target.is( "div" ) ) {
                    $('body').toggleClass('player-android-on');
                }

            });
        }*/

        /* Responsive menu */

        /*$('#dl-menu').dlmenu({
            animationClasses: {
                classin: 'dl-animate-in-2',
                classout: 'dl-animate-out-2'
            }
        });*/


        


        var blognetwork_cover_slider;

        /* Featured area sliders */
        $(".blognetwork-featured-slider").each(function() {
            blognetwork_cover_slider = $(this);
            blognetwork_cover_slider.owlCarousel({
                loop: true,
                autoHeight: false,
                autoWidth: false,
                items: 1,
                margin: 0,
                nav: true,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                center: false,
                fluidSpeed: 100,
                mouseDrag: false,
                autoplay: parseInt(blognetwork_js_settings.cover_autoplay) ? true : false,
                autoplayTimeout: parseInt(blognetwork_js_settings.cover_autoplay_time) * 1000,
                autoplayHoverPause: true,
                onChanged: function(elem) {

                    var current = this.$element.find('.owl-item.active');
                    var format_content = current.find('.blognetwork-format-content');

                    if (format_content !== undefined && format_content.children().length !== 0) {

                        var item_wrap = current.find('.blognetwork-featured-item');
                        var cover = item_wrap.find('.blognetwork-cover');

                        if (cover.attr('data-action') == 'audio' || cover.attr('data-action') == 'video') {

                            var cover_bg = current.find('.blognetwork-cover-bg');
                            var inplay = item_wrap.find('.blognetwork-format-inplay');

                            format_content.html('');
                            format_content.fadeOut(300);
                            inplay.fadeOut(300);
                            cover.fadeIn(300);
                            item_wrap.find('.blognetwork-f-hide').fadeIn(300);
                            cover_bg.removeAttr('style');


                        }
                    }

                },
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
            });

        });


        $('.blognetwork-featured-slider-4').owlCarousel({
            stagePadding: 200,
            loop: true,
            margin: 0,
            items: 1,
            center: true,
            nav: true,
            autoWidth: true,
            autoplay: parseInt(blognetwork_js_settings.cover_autoplay) ? true : false,
            autoplayTimeout: parseInt(blognetwork_js_settings.cover_autoplay_time) * 1000,
            autoplayHoverPause: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1,
                    stagePadding: 200
                },
                600: {
                    items: 1,
                    stagePadding: 200
                },
                990: {
                    items: 1,
                    stagePadding: 200
                },
                1200: {
                    items: 1,
                    stagePadding: 250
                },
                1400: {
                    items: 1,
                    stagePadding: 300
                },
                1600: {
                    items: 1,
                    stagePadding: 350
                },
                1800: {
                    items: 1,
                    stagePadding: 768
                }
            }
        });

        /* Module slider */

        $(".blognetwork-slider").each(function() {
            var controls = $(this).closest('.blognetwork-module').find('.blognetwork-slider-controls');
            var module_columns = $(this).closest('.blognetwork-module').attr('data-col');
            var layout_columns = controls.attr('data-col');
            var slider_items = module_columns / layout_columns;
            var autoplay = parseInt(controls.attr('data-autoplay')) ? true : false;
            var autoplay_time = parseInt(controls.attr('data-autoplay-time')) * 1000;

            $(this).owlCarousel({
                rtl: (blognetwork_js_settings.rtl_mode === "true"),
                loop: true,
                autoHeight: false,
                autoWidth: false,
                items: slider_items,
                margin: 40,
                nav: true,
                center: false,
                fluidSpeed: 100,
                autoplay: autoplay,
                autoplayTimeout: autoplay_time,
                autoplayHoverPause: true,
                navContainer: controls,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        margin: 0,
                        items: (layout_columns <= 2) ? 2 : 1
                    },
                    1023: {
                        margin: 36,
                        items: slider_items
                    }
                }
            });
        });

        /* Widget slider */

        $(".blognetwork-widget-slider").each(function() {
            var $controls = $(this).closest('.widget').find('.blognetwork-slider-controls');

            $(this).owlCarousel({
                rtl: (blognetwork_js_settings.rtl_mode === "true"),
                loop: true,
                autoHeight: false,
                autoWidth: false,
                items: 1,
                nav: true,
                center: false,
                fluidSpeed: 100,
                margin: 0,
                navContainer: $controls,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
            });
        });


        /* On window resize-events */

        $(window).resize(function() {
            // Don't do anything in full screen mode
            if(document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement){
                return;
            }
            blognetwork_sticky_sidebar();
            blognetwork_logo_setup();
            blognetwork_sidebar_switch();
            blognetwork_cover_height_fix();
        });


        /* Check if there is colored section below featured area */

        $(".blognetwork-featured-1").each(function() {
            var $featured = $(this);
            var $blognetwork_bg = $(this).next();
            var $blognetwork_bg_color = $blognetwork_bg.css('background-color');

            if ($blognetwork_bg.hasClass('blognetwork-bg')) {
                $featured.css('background-color', $blognetwork_bg_color);
            }
        });



        /* Fitvidjs functionality on single posts */

        //blognetwork_fit_videos($('.blognetwork-single-content .entry-media, .entry-content-single, .blognetwork-single-cover .blognetwork-popup-wrapper'));


        /* Highlight area hovers */

        $(".blognetwork-featured .blognetwork-highlight .entry-title a,.blognetwork-featured .blognetwork-highlight .action-item,.blognetwork-active-hover .entry-title,.blognetwork-active-hover .action-item").hover(function() {
                $(this).siblings().stop().animate({
                    opacity: 0.4
                }, 150);
                $(this).parent().siblings().stop().animate({
                    opacity: 0.4
                }, 150);
                $(this).parent().parent().siblings().stop().animate({
                    opacity: 0.4
                }, 150);
            },
            function() {
                $(this).siblings().stop().animate({
                    opacity: 1
                }, 150);
                $(this).parent().siblings().stop().animate({
                    opacity: 1
                }, 150);
                $(this).parent().parent().siblings().stop().animate({
                    opacity: 1
                }, 150);
            });


        /* Header search */

        $('body').on('click', '.blognetwork-action-search span', function() {

            $(this).find('i').toggleClass('fv-close', 'fv-search');
            $(this).closest('.blognetwork-action-search').toggleClass('active');
            setTimeout(function() {
                $('.active input[type="text"]').focus();
            }, 150);

            if ($('.blognetwork-responsive-header .blognetwork-watch-later, .blognetwork-responsive-header .blognetwork-listen-later').hasClass('active')) {
                $('.blognetwork-responsive-header .blognetwork-watch-later, .blognetwork-responsive-header .blognetwork-listen-later').removeClass('active');
            }

        });

        $('body').on('click', '.blognetwork-responsive-header .blognetwork-watch-later span', function() {

            $(this).closest('.blognetwork-watch-later').toggleClass('active');
            $('body').toggleClass('blognetwork-watch-later-active');
            $('.blognetwork-responsive-header .blognetwork-action-search').removeClass('active').find('i').removeClass('fv-close').addClass('fv-search');
            $('.blognetwork-responsive-header .blognetwork-watch-later.active .sub-menu').css('width', $(window).width()).css('height', $(window).height());


        });

        $('body').on('click', '.blognetwork-responsive-header .blognetwork-listen-later span', function() {

            $(this).closest('.blognetwork-listen-later').toggleClass('active');
            $('.blognetwork-responsive-header .blognetwork-action-search').removeClass('active').find('i').removeClass('fv-close').addClass('fv-search');
            $('.blognetwork-responsive-header .blognetwork-listen-later.active .sub-menu').css('width', $(window).width()).css('height', $(window).height());


        });

        $(document).on('click', function(evt) {
            if (!$(evt.target).is('.blognetwork-responsive-header .blognetwork-action-search')) {
                if ($('.blognetwork-responsive-header').hasClass('blognetwork-res-open')) {
                    $(".blognetwork-responsive-header .dl-trigger").trigger("click");
                }

                $('.blognetwork-responsive-header .blognetwork-action-search.active .sub-menu').css('width', $(window).width());
            }
        });

        /* On images loaded events */

        $('body').imagesLoaded(function() {
            blognetwork_sidebar_switch();
            blognetwork_sticky_sidebar();

        });


        $('.blognetwork-cover-bg:first').imagesLoaded(function() {
            $('.blognetwork-cover').animate({
                opacity: 1
            }, 300);
        });


        /* Share buttons click */

        $('body').on('click', '.blognetwork-share-item', function(e) {
            e.preventDefault();
            var data = $(this).attr('data-url');
            blognetwork_social_share(data);
        });


        /* Hendling url on ajax call for load more and infinite scroll case */
        if ($('.blognetwork-infinite-scroll').length || $('.blognetwork-load-more').length) {

            var blognetwork_url_pushes = [];
            var blognetwork_pushes_up = 0;
            var blognetwork_pushes_down = 0;

            var push_obj = {
                prev: window.location.href,
                next: '',
                offset: $(window).scrollTop(),
                prev_title: window.document.title,
                next_title: window.document.title
            };

            blognetwork_url_pushes.push(push_obj);
            window.history.pushState(push_obj, '', window.location.href);

            var last_up, last_down = 0;

            $(window).scroll(function() {
                if (blognetwork_url_pushes[blognetwork_pushes_up].offset != last_up && $(window).scrollTop() < blognetwork_url_pushes[blognetwork_pushes_up].offset) {

                    last_up = blognetwork_url_pushes[blognetwork_pushes_up].offset;
                    last_down = 0;
                    window.document.title = blognetwork_url_pushes[blognetwork_pushes_up].prev_title;
                    window.history.replaceState(blognetwork_url_pushes, '', blognetwork_url_pushes[blognetwork_pushes_up].prev); //1

                    blognetwork_pushes_down = blognetwork_pushes_up;
                    if (blognetwork_pushes_up != 0) {
                        blognetwork_pushes_up--;
                    }
                }
                if (blognetwork_url_pushes[blognetwork_pushes_down].offset != last_down && $(window).scrollTop() > blognetwork_url_pushes[blognetwork_pushes_down].offset) {

                    last_down = blognetwork_url_pushes[blognetwork_pushes_down].offset;
                    last_up = 0;

                    window.document.title = blognetwork_url_pushes[blognetwork_pushes_down].next_title;
                    window.history.replaceState(blognetwork_url_pushes, '', blognetwork_url_pushes[blognetwork_pushes_down].next);

                    blognetwork_pushes_up = blognetwork_pushes_down;
                    if (blognetwork_pushes_down < blognetwork_url_pushes.length - 1) {
                        blognetwork_pushes_down++;
                    }

                }
            });

        }


        /* Load more button handler */
        var blognetwork_load_ajax_new_count = 0;

        $("body").on('click', '.blognetwork-load-more a', function(e) {

            e.preventDefault();
            var start_url = window.location.href;
            var prev_title = window.document.title;
            var $link = $(this);
            var page_url = $link.attr("href");

            $link.addClass('blognetwork-loader-active');
            $('.blognetwork-loader').show();
            $("<div>").load(page_url, function() {
                var n = blognetwork_load_ajax_new_count.toString();
                var $wrap = $link.closest('.blognetwork-module').find('.blognetwork-posts');
                var $new = $(this).find('.blognetwork-module:last article').addClass('blognetwork-new-' + n);
                var $this_div = $(this);

                $new.imagesLoaded(function() {

                    $new.hide().appendTo($wrap).fadeIn(400);

                    if ($this_div.find('.blognetwork-load-more').length) {
                        $('.blognetwork-load-more').html($this_div.find('.blognetwork-load-more').html());
                        $('.blognetwork-loader').hide();
                        $link.removeClass('blognetwork-loader-active');
                    } else {
                        $('.blognetwork-load-more').fadeOut('fast').remove();
                    }

                    blognetwork_sticky_sidebar();

                    if (page_url != window.location) {

                        blognetwork_pushes_up++;
                        blognetwork_pushes_down++;
                        var next_title = $this_div.find('title').text();

                        var push_obj = {
                            prev: start_url,
                            next: page_url,
                            offset: $(window).scrollTop(),
                            prev_title: prev_title,
                            next_title: next_title
                        };

                        blognetwork_url_pushes.push(push_obj);
                        window.document.title = next_title;
                        window.history.pushState(push_obj, '', page_url);

                    }

                    blognetwork_load_ajax_new_count++;

                    return false;
                });

            });

        });


        /* Infinite scroll handler */
        var blognetwork_infinite_allow = true;

        if ($('.blognetwork-infinite-scroll').length) {
            $(window).scroll(function() {
                if (blognetwork_infinite_allow && $('.blognetwork-infinite-scroll').length && ($(this).scrollTop() > ($('.blognetwork-infinite-scroll').offset().top) - $(this).height() - 200)) {

                    var $link = $('.blognetwork-infinite-scroll a');
                    var page_url = $link.attr("href");
                    var start_url = window.location.href;
                    var prev_title = window.document.title;

                    if (page_url != undefined) {
                        blognetwork_infinite_allow = false;
                        $('.blognetwork-loader').show();
                        $("<div>").load(page_url, function() {
                            var n = blognetwork_load_ajax_new_count.toString();
                            var $wrap = $link.closest('.blognetwork-module').find('.blognetwork-posts');
                            var $new = $(this).find('.blognetwork-module:last article').addClass('blognetwork-new-' + n);
                            var $this_div = $(this);

                            $new.imagesLoaded(function() {

                                $new.hide().appendTo($wrap).fadeIn(400);

                                if ($this_div.find('.blognetwork-infinite-scroll').length) {
                                    $('.blognetwork-infinite-scroll').html($this_div.find('.blognetwork-infinite-scroll').html());
                                    $('.blognetwork-loader').hide();
                                    blognetwork_infinite_allow = true;
                                } else {
                                    $('.blognetwork-infinite-scroll').fadeOut('fast').remove();
                                }

                                blognetwork_sticky_sidebar();

                                if (page_url != window.location) {

                                    blognetwork_pushes_up++;
                                    blognetwork_pushes_down++;
                                    var next_title = $this_div.find('title').text();

                                    var push_obj = {
                                        prev: start_url,
                                        next: page_url,
                                        offset: $(window).scrollTop(),
                                        prev_title: prev_title,
                                        next_title: next_title
                                    };

                                    blognetwork_url_pushes.push(push_obj);
                                    window.document.title = next_title;
                                    window.history.pushState(push_obj, '', page_url);

                                }

                                blognetwork_load_ajax_new_count++;

                                return false;
                            });

                        });
                    }
                }
            });
        }

        /**
         * Check if is set some other laguage and return his language key to fix ajax request
         * @type mixed Sting or Null
         */
        var wpml_current_lang = blognetwork_js_settings.ajax_wpml_current_lang !== null ? '?wpml_lang='+blognetwork_js_settings.ajax_wpml_current_lang : '';
        
        /* Cover format actions */

        $("body").on('click', '.blognetwork-cover', function(e) {

            var action = $(this).attr('data-action');
            var container = $(this).closest('.blognetwork-cover-bg').find('.blognetwork-format-content');
            var item_wrap = $(this).closest('.blognetwork-featured-item');
            var cover_bg = $(this).closest('.blognetwork-cover-bg');
            var inplay = item_wrap.find('.blognetwork-format-inplay');

            if (action == 'video') {

                if (item_wrap.parent().hasClass('owl-item')) {
                    blognetwork_cover_slider.trigger('stop.owl.autoplay');
                }

                var data = {
                    action: 'blognetwork_format_content',
                    format: 'video',
                    display_playlist: $('.blognetwork-single-cover').length ? true : false,
                    id: $(this).attr('data-id')
                };

                var opener = $(this);

                opener.fadeOut(300, function() {
                    container.append('<div class="blognetwork-format-loader"><div class="uil-ripple-css"><div></div><div></div></div></div>');
                    container.fadeIn(300);


                    inplay.find('.container').html('');
                    inplay.find('.container').append(item_wrap.find('.entry-header').clone()).append(item_wrap.find('.entry-actions').clone());

                    $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, data, function(response) {

                        container.find('.blognetwork-format-loader').remove();

                        container.append('<div class="blognetwork-popup-wrapper">' + response + '</div>');

                        blognetwork_fit_videos(container);

                        blognetwork_try_autoplay(container);

                        blognetwork_disable_related_videos(container);

                        item_wrap.find('.blognetwork-f-hide').fadeOut(300);

                        $('body').addClass('blognetwork-in-play');

                        setTimeout(function() {

                            var $playlist_list = $('.blognetwork-playlist');

                            if ($(window).width() > 768) {

                                var set_height = cover_bg.get(0).scrollHeight;

                                if (!$('.blognetwork-popup-wrapper > .blognetwork-cover-playlist-active').length) {
                                    set_height = set_height < 690 ? 690 : set_height;
                                    cover_bg.animate({
                                        height: set_height
                                    }, 300);
                                } else {
                                    cover_bg.css('height', 'auto');
                                    cover_bg.addClass('blognetwork-cover-playlist-active');
                                    item_wrap.addClass('blognetwork-playlist-mode-acitve');
                                }


                                var $video = cover_bg.find('.blognetwork-playlist-video iframe'),
                                    $script = cover_bg.find('.blognetwork-playlist-video script');

                                if (!$video.length && !$script.length) {
                                    $video = cover_bg.find('.blognetwork-playlist-video video');
                                }

                                // Fix for playwire
                                if ($script.length) {
                                    $(document).on('playwire-ready', function() {
                                        $playlist_list.find('.blognetwork-posts').css('height', $script.parent().height() - $playlist_list.find('.blognetwork-mod-head').height() - 25); // Fix for margin bottom
                                    });
                                } else {
                                    $playlist_list.find('.blognetwork-posts').css('height', $video.height() - $playlist_list.find('.blognetwork-mod-head').height() - 25); // Fix for margin bottom
                                }

                            } else {
                                cover_bg.css('height', 'auto');
                                cover_bg.parent().css('height', 'auto');
                                $playlist_list.css('height', 460);
                            }

                        }, 50);

                        inplay.slideDown(300);

                    });

                });

            }

            if (action == 'audio') {

                if (item_wrap.parent().hasClass('owl-item')) {
                    blognetwork_cover_slider.trigger('stop.owl.autoplay');
                }

                var data = {
                    action: 'blognetwork_format_content',
                    format: 'audio',
                    id: $(this).attr('data-id')
                };

                var opener = $(this);


                opener.fadeOut(300, function() {
                    container.append('<div class="blognetwork-format-loader"><div class="uil-ripple-css"><div></div><div></div></div></div>');
                    container.fadeIn(300);

                    item_wrap.find('.blognetwork-f-hide').fadeOut(300);
                    $('body').addClass('blognetwork-in-play');

                    inplay.find('.container').html('');
                    inplay.find('.container').append(item_wrap.find('.entry-header').clone()).append(item_wrap.find('.entry-actions').clone());

                    $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, data, function(response) {

                        //var $response = $($.parseHTML(response));
                        container.find('.blognetwork-format-loader').remove();
                        container.append('<div class="blognetwork-popup-wrapper">' + response + '</div>');
                        blognetwork_fit_videos(container);
                        blognetwork_try_autoplay(container);

                        setTimeout(function() {

                            if ($(window).width() > 768) {

                                var set_height = cover_bg.get(0).scrollHeight;

                                if (!$('.blognetwork-popup-wrapper > .blognetwork-cover-playlist-active').length) {
                                    if (set_height < 690) {
                                        set_height = 690;
                                    }
                                }

                                cover_bg.animate({
                                    height: set_height
                                }, 300);
                            } else {
                                cover_bg.css('height', 'auto');
                                cover_bg.parent().css('height', 'auto');
                            }

                        }, 50);

                        inplay.slideDown(300);


                    });

                });



            }

            if (action == 'gallery') {

                var items = new Array();

                container.find('.gallery-item').each(function() {
                    items.push({
                        src: $(this).find('a').attr('href'),
                        caption: $(this).find('.wp-caption-text.gallery-caption').text()
                    });
                });


                $.magnificPopup.open({
                    items: items,
                    gallery: {
                        enabled: true
                    },
                    type: 'image',
                    image: {
                        titleSrc: function(item) {
                            var $caption = item.data.caption;
                            if ($caption != 'undefined') {
                                return $caption;
                            }
                            return '';
                        }
                    },
                    removalDelay: 120,
                    mainClass: 'mfp-with-fade',
                    closeBtnInside: false,
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="fv fv-close"></i></button>',
                    callbacks: {
                        open: function() {
                            $.magnificPopup.instance.next = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.next.call(self);
                                }, 120);
                            };
                            $.magnificPopup.instance.prev = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.prev.call(self);
                                }, 120);
                            };
                        },
                        imageLoadComplete: function() {
                            var self = this;
                            setTimeout(function() {
                                self.wrap.addClass('mfp-image-loaded');
                            }, 16);
                        }
                    }
                });

            }

            if (action == 'image') {

                var image = $(this).attr('data-image');
                var link = $(this);
                $.magnificPopup.open({
                    items: {
                        src: image,
                    },
                    type: 'image',
                    image: {
                        titleSrc: function(item) {
                            var $caption = link.parent().find('.wp-caption-text');
                            if ($caption !== undefined) {
                                return $caption.text();
                            }
                            return '';
                        }
                    },
                    removalDelay: 120,
                    mainClass: 'mfp-with-fade',
                    closeBtnInside: false,
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="fv fv-close"></i></button>',
                    callbacks: {
                        open: function() {
                            //overwrite default prev + next function. Add timeout for css3 crossfade animation
                            $.magnificPopup.instance.next = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.next.call(self);
                                }, 120);
                            };
                            $.magnificPopup.instance.prev = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.prev.call(self);
                                }, 120);
                            };
                        },
                        imageLoadComplete: function() {
                            var self = this;
                            setTimeout(function() {
                                self.wrap.addClass('mfp-image-loaded blognetwork-f-img');
                            }, 16);
                        }
                    }
                });
            }

        });


        /* Watch Later */

        $("body").on('click', '.action-item.watch-later', function(e) {
            e.preventDefault();

            var container = $('.blognetwork-watch-later');
            var counter = container.find('.blognetwork-watch-later-count');
            var posts = container.find('.blognetwork-menu-posts');
            var empty = container.find('.blognetwork-wl-empty');
            var what = $(this).attr('data-action');

            if (what == 'add') {

                $(this).find('i.fv').removeClass('fv-watch-later').addClass('fv-added');
                counter.text(parseInt(counter.first().text()) + 1);
                $(this).attr('data-action', 'remove');

            } else {

                $(this).find('i.fv').removeClass('fv-added').addClass('fv-watch-later');
                counter.text(parseInt(counter.first().text()) - 1);
                $(this).attr('data-action', 'add');
            }

            if (parseInt(counter.text()) > 0) {
                counter.fadeIn(300);
                empty.fadeOut(300);
            } else {
                counter.fadeOut(300);
                empty.fadeIn(300);
            }

            $(this).find('span').removeClass('hidden');
            $(this).find('span.' + what).addClass('hidden');

            var data = {
                action: 'blognetwork_watch_later',
                what: what,
                id: $(this).attr('data-id')
            };

            $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, data, function(response) {
                posts.html(response);
            });

        });

        $("body").on('click', '.blognetwork-remove-wl', function(e) {
            e.preventDefault();

            var container = $('.blognetwork-watch-later');
            var counter = container.find('.blognetwork-watch-later-count');
            var empty = container.find('.blognetwork-wl-empty');

            counter.text(parseInt(counter.first().text()) - 1);

            $(this).closest('.wl-post').fadeOut(300).remove();

            if (parseInt(counter.text()) == 0) {
                counter.fadeOut(300);
                empty.fadeIn(300);
            }

            var data = {
                action: 'blognetwork_watch_later',
                what: 'remove',
                id: $(this).attr('data-id')
            };

            $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, data, function(response) {
                //posts.html(response);
            });

        });

        /*if (blognetwork_js_settings.watch_later_ajax && $('.blognetwork-watch-later').length) {
            $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, {
                action: 'blognetwork_load_watch_later'
            }, function(response) {
                $('.blognetwork-watch-later').html(response);
            });
        }*/

        /* Listen Later */

        $("body").on('click', '.action-item.listen-later', function(e) {
            e.preventDefault();

            var container = $('.blognetwork-listen-later');
            var counter = container.find('.blognetwork-listen-later-count');
            var posts = container.find('.blognetwork-menu-posts');
            var empty = container.find('.blognetwork-ll-empty');
            var what = $(this).attr('data-action');

            if (what == 'add') {

                $(this).find('i.fv').removeClass('fv-listen-later').addClass('fv-listen-close');
                counter.text(parseInt(counter.first().text()) + 1);
                $(this).attr('data-action', 'remove');

            } else {

                $(this).find('i.fv').removeClass('fv-listen-close').addClass('fv-listen-later');
                counter.text(parseInt(counter.first().text()) - 1);
                $(this).attr('data-action', 'add');
            }

            if (parseInt(counter.text()) > 0) {
                counter.fadeIn(300);
                empty.fadeOut(300);
            } else {
                counter.fadeOut(300);
                empty.fadeIn(300);
            }

            $(this).find('span').removeClass('hidden');
            $(this).find('span.' + what).addClass('hidden');

            var data = {
                action: 'blognetwork_listen_later',
                what: what,
                id: $(this).attr('data-id')
            };

            $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, data, function(response) {
                posts.html(response);
            });

        });

        $("body").on('click', '.blognetwork-remove-ll', function(e) {
            e.preventDefault();

            var container = $('.blognetwork-listen-later');
            var counter = container.find('.blognetwork-listen-later-count');
            var empty = container.find('.blognetwork-ll-empty');

            counter.text(parseInt(counter.first().text()) - 1);

            $(this).closest('.blognetwork-menu-posts').fadeOut(300).remove();

            if (parseInt(counter.text()) == 0) {
                counter.fadeOut(300);
                empty.fadeIn(300);
            }

            var data = {
                action: 'blognetwork_listen_later',
                what: 'remove',
                id: $(this).attr('data-id')
            };

            $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, data, function(response) {
                //posts.html(response);
            });

        });

        if (blognetwork_js_settings.listen_later_ajax && $('.blognetwork-listen-later').length) {
            $.post(blognetwork_js_settings.ajax_url+wpml_current_lang, {
                action: 'blognetwork_load_listen_later'
            }, function(response) {
                $('.blognetwork-listen-later').html(response);
            });
        }


        /* Cinema mode */

        var blognetwork_before_cinema_height;
        var blognetwork_before_cinema_width;

        $("body").on('click', '.action-item.cinema-mode', function(e) {
            e.preventDefault();

            var current_video = $(this).closest('.blognetwork-featured-item').find('.blognetwork-format-content');

            $(window).scrollTop(0);

            $('body').addClass('blognetwork-popup-on');
            current_video.addClass('blognetwork-popup');


            if ($('.blognetwork-featured-slider').length) {
                blognetwork_before_cinema_height = current_video.height();
                blognetwork_before_cinema_width = current_video.width();



                if ($(window).width() > 990) {
                    current_video.height($(window).height()).width($(window).width()).css('top', -$('.blognetwork-site-header').height()).css('marginTop', -$('.blognetwork-site-header').height() / 2);
                } else {
                    current_video.height($(window).height()).width($(window).width()).css('top', -50).css('marginTop', -$('.blognetwork-site-header').height() / 2);
                    $('.blognetwork-responsive-header').css('z-index', 0);
                }


                var current_slide = $('.blognetwork-popup').parent().parent().parent();
                current_slide.attr('data-width', current_slide.width()).height($(window).height()).width($(window).width());



                $('.blognetwork-header-wrapper').css('z-index', 0);

            }

            if ($('.blognetwork-single-content .blognetwork-format-content').length && $(window).width() < 1367) {
                blognetwork_before_cinema_height = current_video.height();
                blognetwork_before_cinema_width = current_video.width();
                current_video.height($(window).height()).width($(window).width());
            }

            if (!current_video.find('.blognetwork-popup-wrapper').length) {
                current_video.closest('.blognetwork-cover-bg').find('.blognetwork-cover').click();
            }

            current_video.append('<a class="blognetwork-popup-close" href="javascript:void(0);"><i class="fv fv-close"></i></a>');
            if ($('.blognetwork-featured-slider').length) {
                $('.blognetwork-popup-close').css('top', $('.blognetwork-site-header').height() - 20);
            }

        });


        /* Close popup */

        $("body").on('click', '.blognetwork-popup-close', function(e) {

            var cover_bg = $(this).closest('.blognetwork-cover-bg');

            if ($('.blognetwork-featured-slider').length) {

                $(this).closest('.blognetwork-format-content').removeAttr('style');
                $('.blognetwork-header-wrapper').css('z-index', 10);
                var current_slide = $('.blognetwork-popup').parent().parent().parent();
                current_slide.removeAttr('style').css('width', current_slide.attr('data-width'));

            }

            if ($('.blognetwork-single-content .blognetwork-format-content').length && $(window).width() < 1367) {
                $(this).closest('.blognetwork-format-content').removeAttr('style');
            }

            $(this).closest('.blognetwork-format-content').removeClass('blognetwork-popup');

            $('body').removeClass('blognetwork-popup-on');
            $('.action-item, .entry-header').removeAttr('style');
            $(this).remove();

            setTimeout(function() {
                var isFF = !!window.sidebar;
                if (isFF == true) {
                    cover_bg.removeAttr('style');
                }

                cover_bg.animate({
                    height: cover_bg.get(0).scrollHeight
                }, 300);



            }, 50);

            if ($(window).width() < 990) {
                $('.blognetwork-responsive-header').removeAttr('style');
            }


        });

        /* Close popup on Escape */

        $(document).keyup(function(ev) {
            if (ev.keyCode == 27 && $('body').hasClass('blognetwork-popup-on')) {

                $('.blognetwork-popup-close').click();

            }
        });


        /* Cover in play mode */
        if ((blognetwork_js_settings.cover_inplay && $('.blognetwork-cover-bg').length && $('.blognetwork-cover-bg').hasClass('video')) || (blognetwork_js_settings.cover_inplay_audio && $('.blognetwork-cover-bg').length && $('.blognetwork-cover-bg').hasClass('audio'))) {

            var cover_bg = $('.blognetwork-cover-bg');
            blognetwork_fit_videos($('.blognetwork-format-content'));
            blognetwork_disable_related_videos(cover_bg);



            var $playlist_list = $('.blognetwork-playlist');

            if ($playlist_list.length) {

                setTimeout(function() {
                    var $video = cover_bg.find('.blognetwork-playlist-video iframe'),
                        $script = cover_bg.find('.blognetwork-playlist-video script');

                    if (!$video.length && !$script.length) {
                        $video = cover_bg.find('.blognetwork-playlist-video video');
                    }

                    // Fix for playwire
                    if (!$script.length) {
                        $playlist_list.find('.blognetwork-posts').css('height', $video.height() - $playlist_list.find('.blognetwork-mod-head').height() - 25);
                    }else{
                        $playlist_list.find('.blognetwork-posts').css('height', $script.parent().height() - $playlist_list.find('.blognetwork-mod-head').height() - 25 ); // Fix for margin bottom
                    }

                    $('.blognetwork-cover-bg').animate({
                        height: cover_bg.get(0).scrollHeight
                    }, 300);
                    $('.blognetwork-format-inplay').slideDown(300);

                }, 500);
            } else {
                $('.blognetwork-cover-bg').animate({
                    height: cover_bg.get(0).scrollHeight
                }, 300);
                $('.blognetwork-format-inplay').slideDown(300);
            }

            /* playwire support */
            setTimeout(function() {
                if (cover_bg.find('iframe').hasClass('zeus_iframe') && $(window).width() > 1240) {
                    var h = cover_bg.find('.blognetwork-popup-wrapper div').height();
                    if (h > 500) {
                        cover_bg.css('height', h + 70);
                    }
                }
            }, 1000);
        }


        $(document).on('playwire-ready', function() {
            //console.log(6);

            $('.blognetwork-cover-playlist-active').css('margin-bottom', '40px');
            $playlist_list.find('.blognetwork-posts').css('height', $script.parent().height() - $playlist_list.find('.blognetwork-mod-head').height() ); // Fix for margin bottom
        });
        /* Reverse submenu ul if out of the screen */

        $('.blognetwork-main-nav li').hover(function(e) {
            if ($(this).closest('body').width() < $(document).width()) {

                $(this).find('ul').addClass('blognetwork-rev');
            }
        }, function() {
            $(this).find('ul').removeClass('blognetwork-rev');
        });

        /* Scroll to comments */

        $('body').on('click', '.blognetwork-single-cover .entry-actions a.comments, .blognetwork-single-cover .meta-comments a, .blognetwork-single-content .meta-comments a:first', function(e) {

            e.preventDefault();
            var target = this.hash,
                $target = $(target);


            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function() {
                window.location.hash = target;
            });

        });


        /* Initialize gallery pop-up */

        blognetwork_popup_gallery($('.blognetwork-site-content'));

        /* Initialize image popup  */

        blognetwork_popup_image($('.blognetwork-content'));


        /* Sticky sidebar */

        function blognetwork_sticky_sidebar() {
           /* if ($(window).width() >= 1024) {
                if ($('.blognetwork-sticky').length) {
                    $('.blognetwork-sidebar').each(function() {
                        var $section = $(this).closest('.blognetwork-section');
                        if ($section.find('.blognetwork-ignore-sticky-height').length) {
                            var section_height = $section.height() - $section.find('.blognetwork-ignore-sticky-height').height();
                        } else {
                            var section_height = $section.height();
                        }

                        $(this).css('min-height', section_height);
                    });
                }
            } else {
                $('.blognetwork-sidebar').each(function() {
                    $(this).css('height', 'auto');
                    $(this).css('min-height', '1px');
                });
            }
            

            $(".blognetwork-sticky").stick_in_parent({
                parent: ".blognetwork-sidebar",
                inner_scrolling: false,
                offset_top: 99
            });
            if ($(window).width() < 1024) {
                $(".blognetwork-sticky").trigger("sticky_kit:detach");
            }*/
            if ($(window).width() >= 1024) {
                $('.blognetwork-sidebar').theiaStickySidebar({
                  // Settings
                  additionalMarginTop: 100
                });
            }
        }

        /* Put sidebars below content in responsive mode */
        function blognetwork_sidebar_switch() {
            $('.blognetwork-sidebar-left').each(function() {
                if ($(window).width() < 992) {
                    $(this).insertAfter($(this).next());
                } else {
                    $(this).insertBefore($(this).prev());
                }
            });
        }


        /* Share popup function */

        function blognetwork_social_share(data) {
            window.open(data, "Share", 'height=500,width=760,top=' + ($(window).height() / 2 - 250) + ', left=' + ($(window).width() / 2 - 380) + 'resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0');
        }


        /* Switch to retina logo */

        var blognetwork_retina_logo_done = false,
            blognetwork_retina_mini_logo_done = false;

        function blognetwork_logo_setup() {

            //Retina logo
            if (window.devicePixelRatio > 1) {

                if (blognetwork_js_settings.logo_retina && !blognetwork_retina_logo_done && $('.blognetwork-logo').length) {
                    $('.blognetwork-logo').imagesLoaded(function() {

                        $('.blognetwork-logo').each(function() {
                            if ($(this).is(':visible')) {
                                var width = $(this).width();
                                $(this).attr('src', blognetwork_js_settings.logo_retina).css('width', width + 'px');
                            }
                        });
                    });

                    blognetwork_retina_logo_done = true;
                }

                if (blognetwork_js_settings.logo_mini_retina && !blognetwork_retina_mini_logo_done && $('.blognetwork-logo-mini').length) {
                    $('.blognetwork-logo-mini').imagesLoaded(function() {
                        $('.blognetwork-logo-mini').each(function() {
                            if ($(this).is(':visible')) {
                                var width = $(this).width();
                                $(this).attr('src', blognetwork_js_settings.logo_mini_retina).css('width', width + 'px');
                            }
                        });
                    });

                    blognetwork_retina_mini_logo_done = true;
                }
            }
        }


        /* Pop-up gallery function */

        function blognetwork_popup_gallery(obj) {
            obj.find('.gallery').each(function() {
                $(this).find('.gallery-icon a').magnificPopup({
                    type: 'image',
                    gallery: {
                        enabled: true
                    },

                    image: {
                        titleSrc: function(item) {
                            var $caption = item.el.closest('.gallery-item').find('.gallery-caption');
                            if ($caption != 'undefined') {
                                return $caption.text();
                            }
                            return '';
                        }
                    },
                    removalDelay: 120,
                    mainClass: 'mfp-with-fade',
                    closeBtnInside: false,
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="fv fv-close"></i></button>',
                    callbacks: {
                        open: function() {
                            $.magnificPopup.instance.next = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.next.call(self);
                                }, 120);
                            };
                            $.magnificPopup.instance.prev = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.prev.call(self);
                                }, 120);
                            };
                        },
                        imageLoadComplete: function() {
                            var self = this;
                            setTimeout(function() {
                                self.wrap.addClass('mfp-image-loaded');
                            }, 16);
                        }
                    }
                });
            });
        }




        /* Popup image function */
        function blognetwork_popup_image(obj) {
            if (obj.find("a.blognetwork-popup-img").length) {

                var popupImg = obj.find("a.blognetwork-popup-img");
                popupImg.find('img').each(function() {
                    var $that = $(this);
                    if ($that.hasClass('alignright')) {
                        $that.removeClass('alignright').parent().addClass('alignright');
                    }
                    if ($that.hasClass('alignleft')) {
                        $that.removeClass('alignleft').parent().addClass('alignleft');
                    }
                });

                popupImg.magnificPopup({
                    type: 'image',
                    gallery: {
                        enabled: true
                    },
                   /* image: {
                        titleSrc: function(item) {
                            return item.el.closest('.wp-caption').find('figcaption').text();
                        }
                    },*/
                    removalDelay: 120,
                    mainClass: 'mfp-with-fade',
                    closeBtnInside: false,
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="fv fv-close"></i></button>',
                    callbacks: {
                        open: function() {
                            $.magnificPopup.instance.next = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.next.call(self);
                                }, 120);
                            };
                            $.magnificPopup.instance.prev = function() {
                                var self = this;
                                self.wrap.removeClass('mfp-image-loaded');
                                setTimeout(function() {
                                    $.magnificPopup.proto.prev.call(self);
                                }, 120);
                            };
                        },
                        imageLoadComplete: function() {
                            var self = this;
                            setTimeout(function() {
                                self.wrap.addClass('mfp-image-loaded');
                            }, 16);
                        }
                    }
                });
            }
        }


        /* Fitvidjs function */
        function blognetwork_fit_videos(obj) {
            //obj.find('iframe').removeAttr('width').removeAttr('height');
            obj.fitVids({
                customSelector: "iframe[src^='https://www.dailymotion.com'], iframe[src^='https://player.twitch.tv'], iframe[src^='https://vine.co'], iframe[src^='https://videopress.com'], iframe[src^='https://www.facebook.com'],iframe[src^='//content.jwplatform.com'],iframe[src^='//fast.wistia.net'],iframe[src^='//www.vooplayer.com'], iframe[src^='http://content.zetatv.com.uy'], iframe[src^='//embed.wirewax.com'], iframe[src^='https://eventopedia.navstream.com'], iframe[src^='http://cdn.playwire.com'], iframe[src*='//www.liveleak.com'], iframe[src^='https://drive.google.com'],iframe[src*='wistia.com'],iframe[src*='//videos.sproutvideo.com']"
            });
        }

        /* Video tweaks to force autoplay if possible */
        function blognetwork_try_autoplay(container) {

            if (container.find('video').length) {
                var video = container.find('video');
                video.attr('autoplay', 'true');
                video.mediaelementplayer();

            } else if (container.find('.flowplayer').length) {
                var video = container.find('.flowplayer');
                video.attr('data-fvautoplay', 'true');

            } else if (container.find('iframe').length) {
                var video = container.find('iframe');
                if (video.attr('src').match(/\?/gi)) {
                    video.attr('src', video.attr('src') + '&autoplay=1&auto_play=1');
                } else {
                    video.attr('src', video.attr('src') + '?autoplay=1&auto_play=1');
                }

            } else if (container.find('script').length) {
                var video = container.find('script');
                video.attr('data-onready', 'blognetwork_playwire');
                video.attr('data-id', 'blognetwork_playwire');

            } else if (container.find('audio').length) {
                container.find('audio').attr('autoplay', 'true');
                container.find('audio').mediaelementplayer();
            }

        }

        function blognetwork_disable_related_videos(container) {
            if (blognetwork_js_settings.video_disable_related && container.find('iframe').length) {
                var video = container.find('iframe');
                if (video.attr('src').match(/\?/gi)) {
                    video.attr('src', video.attr('src') + '&rel=0');
                } else {
                    video.attr('src', video.attr('src') + '?rel=0');
                }
            }
        }


        if (blognetwork_js_settings.video_display_sticky && $('.blognetwork-format-content.video').length) {

            var $formatContent = $('.blognetwork-format-content.video');
            $formatContent.prepend(
                '<div class="blognetwork-video-sticky-header clearfix">\n' +
                '    <span class="widget-title h5">' + blognetwork_js_settings.video_sticky_title + '</span>\n' +
                '    <a id="blognetwork-video-sticky-close" href="javascript:void(0)"><span class="fv fv-close"></span></a>\n' +
                '</div>');

            var sticky_video_state = false;

            $(window).scroll(function() {

                var $iframe = $('.blognetwork-format-content.video iframe, .blognetwork-format-content.video video');
                if ($iframe.length < 1 || $(window).width() < 1200 || $formatContent.hasClass('blognetwork-ignore-sticky')) return;

                var bottom = $iframe.position().top + $iframe.outerHeight(true),
                    scroll = $(window).scrollTop();

                if (!sticky_video_state && bottom < scroll) {
                    $formatContent.addClass('blognetwork-sticky-video');
                    sticky_video_state = true;
                    setTimeout(function() {
                        $formatContent.addClass('blognetwork-sticky-animation');
                    }, 300);
                }

                if (sticky_video_state && bottom > scroll) {
                    $formatContent.removeClass('blognetwork-sticky-video').removeClass('blognetwork-sticky-animation');
                    sticky_video_state = false;
                }
            });
        }

        $('body').on('click', '#blognetwork-video-sticky-close', function(e) {
            e.preventDefault();
            $('.blognetwork-format-content').removeClass('blognetwork-sticky-video').addClass('blognetwork-ignore-sticky');
        });

        
        /* Cover Area - custom content layout wrapper height improvements */
        
        blognetwork_cover_height_fix();

        function blognetwork_cover_height_fix(){

            if ( !$('.blognetwork-featured-custom').length ) return;

            var custom_area = $('.blognetwork-featured-custom .blognetwork-featured-item');
            var custom_area_height = custom_area.height();
            var custom_content_height = custom_area.find('.blognetwork-featured-info-custom').height();

            if ( custom_content_height + 140 > custom_area_height ) {
                custom_area.attr('style', 'height: ' + ( custom_content_height + 140 ) + 'px !important');
                custom_area.find('.blognetwork-cover-bg').attr('style', 'height: ' + ( custom_content_height + 140 ) + 'px !important');
           }
        }



        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

            $("body").on("touchstart", ".mks_accordion_heading", function(e) {
                mks_accordion_handle($(this));
            });

            $("body").on("touchstart", ".mks_toggle_heading", function(e) {
                mks_toggle_handle($(this));
            });


            $("body").on("touchstart", ".mks_tabs_nav .mks_tab_nav_item", function(e) {
                mks_tab_handle($(this));
            });

        } else {

            $("body").on("click", ".mks_accordion_heading", function(e) {
                mks_accordion_handle($(this));
            });


            $("body").on("click", ".mks_toggle_heading", function(e) {
                mks_toggle_handle($(this));
            });


            $("body").on("click", ".mks_tabs_nav .mks_tab_nav_item", function(e) {
                mks_tab_handle($(this));
            });
        }

        /* Initialize tabs */
        $('.mks_tabs').each(function() {

            var tabs_nav = $(this).find('.mks_tabs_nav');

            $(this).find('.mks_tab_item').each(function() {
                tabs_nav.append('<div class="mks_tab_nav_item">' + $(this).find('.nav').html() + '</div>');
                $(this).find('.nav').remove();

            });

            $(this).find('.mks_tabs_nav').find('.mks_tab_nav_item:first').addClass('active');
            $(this).find('.mks_tab_item').hide();
            $(this).find('.mks_tab_item:first').show();
            $(this).show();

        });




    }); //document ready end


function mks_accordion_handle($obj) {
        var toggle = $obj.parent('.mks_accordion_item');
        if (!toggle.hasClass('mks_accordion_active')) {
            toggle.parent('div').find('.mks_accordion_item').find('.mks_accordion_content:visible').slideUp("fast");
            toggle.parent('div').find('.mks_accordion_active').removeClass('mks_accordion_active');
            toggle.find('.mks_accordion_content').slideToggle("fast", function() {
                toggle.addClass('mks_accordion_active');
                if ((toggle.offset().top + 100) < $(window).scrollTop()) {
                    $('html, body').stop().animate({
                        scrollTop: (toggle.offset().top - 100)
                    }, '300');
                }
            });
        } else {
            toggle.parent('div').find('.mks_accordion_item').find('.mks_accordion_content:visible').slideUp("fast");
            toggle.parent('div').find('.mks_accordion_active').removeClass('mks_accordion_active');
        }
    }

    function mks_toggle_handle($obj) {
        var toggle = $obj.parent('.mks_toggle');
        toggle.find('.mks_toggle_content').slideToggle("fast", function() {
            toggle.toggleClass('mks_toggle_active');
        });
    }

    function mks_tab_handle($obj) {
        if ($obj.hasClass('active') == false) {

            tab_to_show = $obj.parent('.mks_tabs_nav').find('.mks_tab_nav_item').index($obj);

            $obj.parent('.mks_tabs_nav').parent('.mks_tabs').find('.mks_tab_item').hide();
            $obj.parent('.mks_tabs_nav').parent('.mks_tabs').find('.mks_tab_item').eq(tab_to_show).show();

            $obj.parent('.mks_tabs_nav').find('.mks_tab_nav_item').removeClass('active');
            $obj.addClass('active');

        }
    }



})(jQuery);

/*function blognetwork_playwire() {
    Bolt.playMedia('blognetwork_playwire');
    $(document).trigger('playwire-ready');
}*/

