<?php
if($_POST)
{
    $to_Email       = "mluisaverdu@gmail.com"; //Replace with recipient email address
    $subject        = 'Correo enviado por visitante desde marialuisaverdu.es'; //Subject line for emails


    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {

        //exit script outputting json data
        $output = json_encode(
        array(
            'type'=>'error',
            'text' => 'Request must come from Ajax'
        ));

        die($output);
    }

    //check $_POST vars are set, exit if any missing
    if(!isset($_POST["userEmail"]) || !isset($_POST["userName"]) || !isset($_POST["userMessage"]))
    {
        $output = json_encode(array('type'=>'error', 'text' => 'Input fields are empty!'));
        die($output);
    }

    //Sanitize input data using PHP filter_var().
    $user_Email       = filter_var($_POST["userEmail"], FILTER_SANITIZE_EMAIL);
    $user_Name       = filter_var($_POST["userName"], FILTER_SANITIZE_STRING);
    $user_Message     = filter_var($_POST["userMessage"], FILTER_SANITIZE_STRING);

    //additional php validation
    if(!filter_var($user_Email, FILTER_VALIDATE_EMAIL)) //email validation
    {
        $output = json_encode(array('type'=>'error', 'text' => 'Por favor introduce una dirección de correo valida'));
        die($output);
    }
    if(strlen($user_Message)<5) //check emtpy message
    {
        $output = json_encode(array('type'=>'error', 'text' => 'Mensaje demasiado corto. Por favor escribe algo'));
        die($output);
    }
    if(strlen($user_Name)<4) //check emtpy message
    {
        $output = json_encode(array('type'=>'error', 'text' => 'Nombre demasiado corto. Por favor escribe algo'));
        die($output);
    }

    //proceed with PHP email.

    /*
    Incase your host only allows emails from local domain,
    you should un-comment the first line below, and remove the second header line.
    Of-course you need to enter your own email address here, which exists in your cp.
    */
    //$headers = 'From: your-name@YOUR-DOMAIN.COM' . "\r\n" .
    $headers = 'From: '.$user_Email.'' . "\r\n" . //remove this line if line above this is un-commented
    'Reply-To: '.$user_Email.'' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

        // send mail
    $sentMail = @mail($to_Email, $subject, $user_Message .'  - '.$user_Name, $headers);

    if(!$sentMail)
    {
        $output = json_encode(array('type'=>'error', 'text' => 'Error al enviar el correo. Como alternativa, contáctenos por teléfono'));
        die($output);
    }else{
        $output = json_encode(array('type'=>'message', 'text' => 'Hola '.$user_Name .'. Gracias por contactar, intentaremos responderle a lo largo del día.'));
        die($output);
    }
}
?>
